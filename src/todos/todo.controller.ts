import { AuthGuard } from './../auth/auth.guard';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Request,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { TodoService } from './todo.service';
import { Todos } from './todo.entity';
import { CreateTodoDTO, UpdateTodoDTO } from './dto/todo.dto';

@Controller('todos')
export class TodoController {
  constructor(private todoService: TodoService) {}

  @Get()
  @UseGuards(AuthGuard)
  async getTodos(@Request() req): Promise<Todos[]> {
    const todos = await this.todoService.todosByUserId(req.user.userId);
    return todos;
  }

  @Post()
  @UseGuards(AuthGuard)
  async createTodo(
    @Body(new ValidationPipe()) CreateTodoDTO: CreateTodoDTO,
    @Request() req,
  ): Promise<Todos> {
    const data = { name: CreateTodoDTO.name, userId: req.user.userId };
    const todo = await this.todoService.createTodo(data);
    return todo;
  }
  @Put(':id')
  @UseGuards(AuthGuard)
  async updateTodo(
    @Param('id') id: number,
    @Body(new ValidationPipe()) updateTodoDTO: UpdateTodoDTO,
  ): Promise<Todos> {
    const { name, status } = updateTodoDTO;
    return await this.todoService.updateTodo({
      id,
      name,
      status: status == 1 ? true : false,
    });
  }

  @Delete(':id')
  @UseGuards(AuthGuard)
  async deleteTodo(@Param('id') id: number) {
    const result = await this.todoService.deleteTodo(id);
    return result.affected != 0
      ? { status: true, message: 'Delete success' }
      : { status: false, message: 'An error has occurred' };
  }
}
