import { UsersService } from './../users/user.service';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Todos } from './todo.entity';
import { DeleteResult, Repository } from 'typeorm';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todos) private todoRepository: Repository<Todos>,
    private readonly UsersService: UsersService,
  ) {}

  findAll(): Promise<Todos[]> {
    return this.todoRepository.find();
  }
  findOneById(id: number): Promise<Todos> {
    return this.todoRepository.findOneBy({ id });
  }
  async createTodo(todo: { name: string; userId: number }): Promise<Todos> {
    const { userId, name } = todo;
    const user = await this.UsersService.findOne(userId);
    const data = { name, user };
    return this.todoRepository.save(data);
  }

  todosByUserId(userId: number): Promise<Todos[]> {
    return this.todoRepository.findBy({ userId });
  }

  async updateTodo(todo: {
    id: number;
    name?: string;
    status?: boolean;
  }): Promise<Todos> {
    const { id, name, status } = todo;
    const todoExist = await this.todoRepository.findOneBy({ id });
    if (name) todoExist.name = name;
    if (status) todoExist.status = status;
    return this.todoRepository.save(todoExist);
  }

  deleteTodo(id: number): Promise<DeleteResult> {
    return this.todoRepository.delete({ id });
  }
}
