import { IsNumberString, IsString } from 'class-validator';

export class CreateTodoDTO {
  @IsString()
  name: string;
}

export class UpdateTodoDTO {
  @IsString()
  name?: string;
  @IsNumberString()
  status?: number;
}
