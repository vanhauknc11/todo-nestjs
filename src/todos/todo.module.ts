import { UsersService } from './../users/user.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Todos } from './todo.entity';
import { TodoService } from './todo.service';
import { TodoController } from './todo.controller';
import { UsersModule } from 'src/users/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([Todos]), UsersModule],
  providers: [TodoService],
  controllers: [TodoController],
})
export class TodoModule {}
