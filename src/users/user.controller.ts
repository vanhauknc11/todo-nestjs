import { AuthGuard } from './../auth/auth.guard';
import { ValidationPipe } from './../validation.pipe';
import { LoginResult, ResultInterface } from './../interfaces';
import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { User } from './user.entity';
import { UsersService } from './user.service';
import { CreateUserDTO, LoginDTO } from './dto/user.dto';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

@Controller('users')
export class UserController {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}
  @Get()
  @UseGuards(AuthGuard)
  async user(): Promise<User[]> {
    return this.usersService.findAll();
  }
  @Post('/register')
  async createUser(
    @Body(new ValidationPipe()) CreateUserDto: CreateUserDTO,
  ): Promise<ResultInterface> {
    const { username, password, re_password } = CreateUserDto;

    const user = await this.usersService.findOneByUserName(username);

    if (user) {
      return { status: false, message: 'Username is already exist' };
    }
    if (password !== re_password) {
      return { status: false, message: 'Re Password not same' };
    }
    const saltOrRounds = 10;
    const hash = await bcrypt.hash(password, saltOrRounds);

    const result = await this.usersService.create({ username, password: hash });
    if (!result) return { status: false, message: 'An error has occurred' };
    return { status: true, message: 'Your account has been created !!' };
  }
  @Post('/login')
  async login(
    @Body(new ValidationPipe()) LoginDTO: LoginDTO,
  ): Promise<LoginResult> {
    const { username, password } = LoginDTO;

    const user = await this.usersService.findOneByUserName(username);
    if (!user) return { status: false, message: 'User not exist' };
    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch)
      return { status: false, message: 'Username or password is incorrect' };

    const payload = { username: user.username, userId: user.id };

    return {
      status: true,
      message: 'Login success',
      token: await this.jwtService.signAsync(payload),
    };
  }
}
