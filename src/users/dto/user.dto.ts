import { IsString } from 'class-validator';

export class CreateUserDTO {
  @IsString()
  username: string;
  @IsString()
  password: string;
  @IsString()
  re_password: string;
}

export class LoginDTO {
  @IsString()
  username: string;
  @IsString()
  password: string;
}
