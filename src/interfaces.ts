export interface ResultInterface {
  status: boolean;
  message: string;
}
export interface LoginResult extends ResultInterface {
  token?: string;
}
